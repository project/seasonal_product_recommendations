This module allows you to deliver a more personalized experience to your customers by sending product recommendations on the basis of the prevalent season to them. This will take the customer experience on your eCommerce website to the next level. Customers from different locations have different needs. Then why not customize the recommendations for them, accordingly.

Consider this example: A user living in tropical areas will never be interested in overcoats. It is rather a great idea to recommend him the products that suits best according to the weather in his location. This module will engage users more with your website.

Features for Admin:

This module allows Admin to assign multiple seasons to products of his choice and those products will be showcased to users as per the local season of their location.

Features for Customers:

1) More relevant product recommendations.
2) Products shown will be as per local seasons.
3) No need to browse products because they would be shown on the desired page only, just click and buy.


Working:

This module creates a taxonomy named "season", which contains terms for the list of seasons. You can add/remove seasons from this taxonomy. It creates an entity reference field and adds it to all the product types.

Also, it creates a view "seasonal_product_recommendations" that displays products on the basis of the prevalent season (weather) to those products.

You can configure the seasons by navigating to (Commerce >> Configuration >> Recommendations) through the administration panel.
Add the season relevant to the product, and those products will be displayed under the custom block created by the module. Place the block as per your needs.
